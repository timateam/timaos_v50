#include "types.h"
#include "SDL_screen.h"

////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////

uint16_t lcd_driver_width( void )
{
    return _USE_SCREEN_WIDTH;
}

uint16_t lcd_driver_height( void )
{
    return _USE_SCREEN_HEIGHT;
}

void * lcd_driver_get_buffer( void )
{
    return sdl_GetTarget();
}

void lcd_driver_release_frame( void )
{
}

int lcd_driver_read_mouse( uint16_t * posx, uint16_t * posy, uint16_t * posz )
{
    return 0;
}

int lcd_driver_init( void )
{
    sdl_lcd_init();
    return 0;
}

void lcd_driver_process( void )
{
}

